<?php

//admin form
function jta_admin_settings(){
  $options=db_query("SELECT p.jtaid,p.opt_name,p.title,p.enabled,p.description FROM {jtweetsanywhere_options} p ORDER BY p.enabled DESC, p.title ASC");
  $form = array();
  $checkboxes = array();
  $enabled = array();
  $enabled[] = 0; //zero index is ignored 
  
  while($option = db_fetch_array($options)){
    $checkboxes[$option['jtaid']] = '';
    $form[$option['jtaid']]['title'] = array(
      '#value' => $option['title'],
    );
    $form[$option['jtaid']]['description'] = array(
      '#value' => $option['description'],
    );
    if($option['enabled'] == 1){
		$enabled[] = $option['jtaid'];
	}
  }
  
  $form['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#options' => $checkboxes, 
    '#default_value' => $enabled
  );
  
      $form['notes'] = array(
       '#type' => 'markup',
       '#title' => t('Description'),
       '#value' => 'Notes:<br/>See <a href="http://thomasbillenstein.com/jTweetsAnywhere/#jta_documentation">documentation</a> for more details om configuration oprions.<br/>'.
       'Twitter @Anywhere features (like Hovercards, Tweet Box, Follow Button, etc.) require registration with twitter api, which is not yet implemented</br>'
    );
    
  $form['#theme'][] = 'jtweetsanywhere_admin_form_theme'; 
  $form['#submit'][] = 'jta_admin_settings_submit';
  $form['submit'] = array(
		'#type' => 'submit',
	    '#value' => t('Submit'),
		);
  
    return $form;
  }
  

function jta_admin_settings_validate($form, &$form_state) {
	//TODO: Check the values are good
}

function jta_admin_settings_submit($form, &$form_state) {
	foreach($form_state['values']['checkboxes'] as $key => $value){
	   db_query("UPDATE {jtweetsanywhere_options} SET enabled = %d  WHERE jtaid = %d ", $value == 0 ? 0 : 1, $key);
     }
}

function jta_add_option(){
	$form = array();
   $form['opt_name'] = array(
       '#type' => 'textfield',
       '#title' => t('Option name'),
       '#default_value' => '',
       '#description' => t('Option name as defined by jtweets')
    );
    $form['title'] = array(
       '#type' => 'textfield',
       '#title' => t('Title'),
       '#default_value' => '',
       '#description' =>  t('Option title')
    );
    $form['enabled'] = array(
       '#type' => 'select',
       '#title' => t('Enabled'),
       '#options' => array(0,1)
    );
    $form['default_value'] = array(
       '#type' => 'textfield',
       '#title' => t('Default value'),
       '#default_value' => '',
    );
    $form['container'] = array(
       '#type' => 'textfield',
       '#title' => t('Container'),
       '#default_value' => '',
       '#description' => t('Name of the main object container')
    );
    $form['subcontainer'] = array(
       '#type' => 'textfield',
       '#title' => t('Subcontainer'),
       '#default_value' => '',
    );
    $form['field_type'] = array(
       '#type' => 'textfield',
       '#title' => t('Field type'),
       '#default_value' => '',
       '#description' => t('Curently always textfield')
    );
    $form['description'] = array(
       '#type' => 'textfield',
       '#title' => t('Description'),
       '#default_value' => '',
       '#description' => t('What this option does; list options')
    );
    
    $form['#submit'][] = 'jta_add_option_submit';
      
    $form['submit'] = array(
       '#type'=> 'submit',
       '#value' => 'Submit'
       );
       return $form;
}

function jta_add_option_validate($form, &$form_state) {
	//TODO: Validate
 }



function jta_add_option_submit($form, &$form_state) {
	   db_query("INSERT INTO {jtweetsanywhere_options} (opt_name,title,enabled, default_value, container, subcontainer, field_type, description) VALUES ('%s', '%s', %d, '%s', '%s', '%s', '%s', '%s')",
	                 check_plain($form_state['values']['opt_name']),
	                 check_plain($form_state['values']['title']),
	                 check_plain($form_state['values']['enabled']),
	                 check_plain($form_state['values']['default_value']),
	                 check_plain($form_state['values']['container']),
	                 check_plain($form_state['values']['subcontainer']),
	                 check_plain($form_state['values']['field_type']),
	                 check_plain($form_state['values']['description'])
	            );
	   drupal_set_message('Configuration has been saved','status');
}


function _jta_build_conf_form(&$form,$conf){
	//we only want enabled options on this form
	$enabled = db_query("SELECT jtaid, opt_name,field_type,title,default_value,description FROM {jtweetsanywhere_options} WHERE enabled = '%d'",1);
	while($item = db_fetch_object($enabled)){
	  $form[$item -> jtaid] = array( //opt_name is not unique so we use jtaid
       '#type' => $item -> field_type,
       '#title' => t($item -> title),
       //$conf is data from panel?
       '#default_value' => isset($conf[$item->jtaid]['value']) ?  $conf[$item->jtaid]['value'] : $item->default_value,
       '#description' => $item -> description
     );
    }
    return $form;
}

function _jta_build_config_data_structure($data){
	// also need to know the parents of data elements
	//but $edit array from block also contains unrelevant data so need to get enabled options from database:
	$enabled = db_query("SELECT * FROM {jtweetsanywhere_options} WHERE enabled = %d", 1);
	$conf=array();
	while ($en = db_fetch_object($enabled)) {
	//check whether this option belongs to a container (object)
	//if the value is blank just ignore it
	if($data[$en->jtaid] == '') continue;
	if($en->container != ''){
		//set up an array if it doesn't already exist
		if (!isset($conf[$en->container])){
		   $conf[$en->container] = array();
	    }
	    //does this option have sub-container? (object within object)
		if($en->subcontainer != ''){
			if (!isset($conf[$en->container][$en->subcontainer])){
			$conf[$en->container][$en->subcontainer] = array();
		    }
		    $conf[$en->container][$en->subcontainer][$en->opt_name] = check_plain($data[$en->jtaid]);
	    } else {
	        $conf[$en->container][$en->opt_name] = check_plain($data[$en->jtaid]);
	    }
	} else {
		
		$conf[$en->opt_name] = check_plain($data[$en->jtaid]);
	    }
    }
    $js_options = _jta_build_js($conf);
    return $js_options;
}

//creates simple array of data ignoring the structure required by jtweetsanywhere for easy access
function _jta_build_config_data($data){
	//but $edit array from blocs also contains unrelevant data so need to get enabled options from database:
	$enabled = db_query("SELECT * FROM {jtweetsanywhere_options} WHERE enabled = %d", 1);
	$conf=array();
	while ($en = db_fetch_object($enabled)) {
	    //does this option have sub-container? (object within object)
	     $conf[$en->jtaid] =  array();
		 $conf[$en->jtaid]['opt_name'] = $en->opt_name;
		 $conf[$en->jtaid]['value'] =  check_plain($data[$en->jtaid]);
		 $conf[$en->jtaid]['title'] =  $en->title;
		 $conf[$en->jtaid]['id'] =  $en->jtaid; //we need it here as well
	 }
    return $conf;
}


function _jta_build_js($conf,&$collector = array()){
	while($item = current($conf)) {
		$collector[] = key($conf) . ' : ';
		if(is_array($item)){
			$collector[] = ' { ';
			_jta_build_js($item,$collector);
			$collector[] = ' } ';
			$collector[] = ',';
	    } else {
			$add_quotes = is_numeric($item) || ($item == 'true') || ($item == 'false');
	    	$collector[] = $add_quotes ? "" : "'" ;
		    $collector[] = $item;
		    $collector[] = $add_quotes ? "" : "'" ;
		    $collector[] = ', ';
	}
	next($conf);
   }
    array_pop($collector);
	return  $collector;
}
    ?>
