<?php

include_once(dirname(__FILE__).'/../../../jtweetsanywhere.admin.inc');

$plugin = array(
  // only provides a single content type
  'single' => TRUE,
  'render last' => TRUE,
  'title' => t('JtweetsAnywhere Panel'),
  'icon' => 'jtweetsanywhere.png',
  'description' => t('Jtweetsanywhere panel.'),
  'category' => t('Miscellaneous'),
  // Constructor.
  'content_types' => array('jtweetsanywhere'),
  // The default context.
  //we'll need to get these from db or not at all
  'defaults' => array('jta_twitter_id'=>'','count'=>10,'showfollow'=>0,'showprofileimages'=>0,'refresh'=>60,'new_default'=>'roman'),
  'all contexts' => TRUE,
);

function jtweetsanywhere_jtweetsanywhere_content_type_render($subtype, $conf, $panel_args, &$context) {
    $block = new stdClass();
    $block->module = 'jtweetsanywhere';
    $js_string = _jtweetsanywhere_panel_content($conf);
    $block->content = theme('jtweetsanywhere', $js_string,$conf['jta_twitter_id']);
    $block->delta = $twitter_id;
    $block->delta = 'unknown';
  return $block;
}

function _jtweetsanywhere_panel_content($conf) {
  $jsfiles = _jtweetsanywhere_files();
  //For the moment just using min.js
  if ($jsfiles['min']) {
    drupal_add_js($jsfiles['min']);
  }
  if ($jsfiles['css']) {
    drupal_add_css($jsfiles['css']);
  }
  $js_string = $conf['jta_structure'];
  return $js_string;
}

function jtweetsanywhere_jtweetsanywhere_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" tweets', array('@s' => $conf['jta_twitter_id']));
}

function jtweetsanywhere_jtweetsanywhere_content_type_edit_form(&$form, &$form_state) {
	$conf = $form_state['conf'];
    $form = _jta_build_conf_form(&$form, unserialize($conf['jta_data']));

	return $form;
}
function jtweetsanywhere_jtweetsanywhere_content_type_edit_form_validate(&$form, &$form_state) {
  //TODO: Validate form
 /* if (intval($form_state['values']['refresh']) < 1) {
    form_error($form['refresh'], t('The refresh period must be a number in seconds greater than 1.'));
  }
  if (intval($form_state['values']['count']) < 1) {
    form_error($form['count'], t('The number of tweets must greater than 1.'));
  } */
}
function jtweetsanywhere_jtweetsanywhere_content_type_edit_form_submit(&$form, &$form_state) {
    // we need to store the structure and data as with block
  $form_state['conf']['jta_structure'] =  $jta_js_string =  implode('', _jta_build_config_data_structure($form_state['values']));
  $form_state['conf']['jta_data'] = serialize(_jta_build_config_data($form_state['values']));
  $form_state['conf']['jta_twitter_id'] = $form_state['values'][1]; //we need to make sure twitterid is always #1
}

